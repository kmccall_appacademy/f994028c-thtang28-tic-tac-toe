class Board
  attr_accessor :grid

  def initialize(array = nil)
    @grid = []
    if array == nil
      @grid = Array.new(3) { Array.new(3) }
    else
      @grid = array
    end
  end

  def [](position)
    row, col = position
    @grid[row][col]
  end

  def []=(position, mark)
    row, col = position
    @grid[row][col] = mark
  end

  def place_mark(position, mark)
    i, j = position
    @grid[i][j] = mark
  end

  def empty?(position)
    i, j = position
    @grid[i][j].nil?
  end

  def over?
    @grid.flatten.none? { |mark| mark.nil?} || winner
  end

  def winner
    (@grid + col + diag).each do |check|
      return :X if check == [:X, :X, :X]
      return :O if check == [:O, :O, :O]
    end
    nil
  end

  def diag
    left_diag = [[0,0], [1,1], [2,2]]
    right_diag = [[0,2], [1,1], [2,0]]

    [left_diag, right_diag].map do |diag|
      diag.map { |row, col| @grid[row][col] }
    end
  end

  def col
    col = [[],[],[]]
    @grid.each do |row|
      row.each_with_index { |mark, idx| col[idx] << mark}
    end
    col
  end
end
