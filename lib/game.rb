require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @board = Board.new
    @player_one = player_one
    @player_two = player_two
    @current_player = @player_one
    @current_player_mark = :X
  end

  def play
    current_player.display(board)
  end

  def play_turn
    move = @current_player.get_move
    @board.place_mark(move, @current_player_mark)
    switch_players!
    @current_player.display(board)
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
      @current_player_mark = :O
    elsif @current_player == @player_two
      @current_player = @player_one
      @current_player_mark = :X
    end
  end
end

# game = Game.new(player_one, player_two)
# game.play
