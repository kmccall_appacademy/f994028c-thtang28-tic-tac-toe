class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        position = [row, col]
        moves << position if board[position].nil?
      end
    end

    moves.each do |position|
      return position if winner(position)
    end

    moves.sample
  end

  def winner(position)
    board[position] = mark
    if board.winner == mark
      board[position] = nil
      true
    else
      board[position] = nil
      false
    end
  end
end
